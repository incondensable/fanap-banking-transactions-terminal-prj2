package com.fanap.convertor;

import com.fanap.repository.Result;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class XmlGenerator {
    private final String filePath = "/Users/incondensable/Desktop/prj2terminal/resource/response.xml";

    public void resultToXml(List<Result> resultList) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = document.createElement("resultSet");
            document.appendChild(root);

            Element results = document.createElement("results");
            root.appendChild(results);

            for (int i = 0; i < resultList.size(); i++ ) {
                Element result = document.createElement("result");
                results.appendChild(result);

                Element customer = document.createElement("customer");
                customer.appendChild(
                        document.createTextNode(
                                resultList.get(i).getCustomer()));
                result.appendChild(customer);

                Element customerId = document.createElement("customerId");
                customerId.appendChild(
                        document.createTextNode(
                                String.valueOf(
                                        resultList.get(i).getCustomerId())));
                result.appendChild(customer);

                Element initialBalance = document.createElement("initialBalance");
                initialBalance.appendChild(
                        document.createTextNode(
                                resultList.get(i).getInitialBalance()));
                result.appendChild(initialBalance);

                Element transactionId = document.createElement("transactionId");
                transactionId.appendChild(
                        document.createTextNode(
                                String.valueOf(
                                        resultList.get(i).getTransactionId())));
                result.appendChild(transactionId);

                Element type = document.createElement("type");
                type.appendChild(
                        document.createTextNode(
                                resultList.get(i).getType()));
                result.appendChild(type);

                Element amount = document.createElement("amount");
                amount.appendChild(
                        document.createTextNode(
                                String.valueOf(
                                        resultList.get(i).getAmount())));
                result.appendChild(amount);

                Element rsCode = document.createElement("rsCode");
                rsCode.appendChild(
                        document.createTextNode(
                                resultList.get(i).getRsCode()));
                result.appendChild(rsCode);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(filePath));

            transformer.transform(domSource, streamResult);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
