package com.fanap.convertor;

import com.fanap.logger.LoggerConfig;
import com.fanap.model.Terminal;
import com.fanap.model.Transactions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class XmlConvertor {
    private final Logger logger = LogManager.getLogger(XmlConvertor.class);
    private Terminal terminal;

    public List<Transactions> xmlParser() {
        LoggerConfig.initializeLogger();
        terminal = new Terminal();

        List<Transactions> transactionsList = new ArrayList<>();

        try {
            File filePath = new File("/Users/incondensable/Desktop/prj2terminal/resource/terminal.xml");
            logger.debug(filePath.getName() + " has successfully gotten");

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(filePath);
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("terminal");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element terminalElement = (Element) nodeList.item(i);
                int id = Integer.parseInt(terminalElement.getAttribute("id"));
                String type = terminalElement.getAttribute("type");
                terminal.setId(id);
                terminal.setType(type);

                NodeList serverNodeList = document.getElementsByTagName("server");
                for (int j = 0; j < serverNodeList.getLength(); j++) {
                    Element serverElement = (Element) serverNodeList.item(j);
                    String ip = serverElement.getAttribute("ip");
                    int port = Integer.parseInt(serverElement.getAttribute("port"));
                    terminal.setIp(ip);
                    terminal.setPort(port);

                    NodeList outLogNodeList = document.getElementsByTagName("outLog");
                    for (int k = 0; k < outLogNodeList.getLength(); k++) {
                        Element outLogElement = (Element) outLogNodeList.item(k);
                        String path = outLogElement.getAttribute("path");
                        terminal.setPath(path);

                        NodeList transactionsNodeList = document.getElementsByTagName("transactions");
                        for (int l = 0; l < transactionsNodeList.getLength(); l++) {

                            NodeList transactionNodeList = document.getElementsByTagName("transaction");
                            for (int m = 0; m < transactionNodeList.getLength(); m++) {
                                Element transactionElement = (Element) transactionNodeList.item(m);
                                int transactionId = Integer.parseInt(transactionElement.getAttribute("id"));
                                String transactionType = transactionElement.getAttribute("type");
                                BigInteger amount = BigInteger.valueOf(Long.parseLong(transactionElement.getAttribute("amount")));
                                BigInteger deposit = BigInteger.valueOf(Long.parseLong(transactionElement.getAttribute("deposit")));

                                Transactions transactions = new Transactions();
                                transactions.setId(transactionId);
                                transactions.setType(transactionType);
                                transactions.setAmount(amount);
                                transactions.setDeposit(deposit);

                                transactionsList.add(transactions);
                            }
                        }
                    }
                }
            }
            logger.debug("xml file successfully got parsed");

        } catch (Exception e) {
            logger.debug(e);
        }
        return transactionsList;
    }

    public Terminal getTerminal() {
        return terminal;
    }
}
