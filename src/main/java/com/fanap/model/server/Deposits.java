package com.fanap.model.server;

import java.math.BigInteger;

public class Deposits {
    private String customer;
    private BigInteger id;
    private String initialBalance;
    private String upperBound;

    Deposits(String customer, BigInteger id, String initialBalance, String upperBound) {
        this.customer = customer;
        this.id = id;
        this.initialBalance = initialBalance;
        this.upperBound = upperBound;
    }

    public String getCustomer() {
        return customer;
    }

    public BigInteger getId() {
        return id;
    }

    public String getInitialBalance() {
        return initialBalance;
    }

    public String getUpperBound() {
        return upperBound;
    }

    public static class Builder {
        private String customer;
        private BigInteger id;
        private String initialBalance;
        private String upperBound;

        public Deposits.Builder setCustomer(String customer) {
            this.customer = customer;
            return this;
        }

        public Deposits.Builder setId(BigInteger id) {
            this.id = id;
            return this;
        }

        public Deposits.Builder setInitialBalance(String initialBalance) {
            this.initialBalance = initialBalance;
            return this;
        }

        public Deposits.Builder setUpperBound(String upperBound) {
            this.upperBound = upperBound;
            return this;
        }

        public Deposits build() {
            return new Deposits(customer, id, initialBalance, upperBound);
        }
    }
}
