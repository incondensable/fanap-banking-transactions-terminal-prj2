package com.fanap.repository;

import java.io.Serializable;
import java.math.BigInteger;

public class Result implements Serializable {
    private static final long serialVersionUID = 5329735277194080364L;

    private String customer;
    private BigInteger customerId;
    private String initialBalance;
    private int transactionId;
    private String type;
    private BigInteger amount;
    private String rsCode;

    public Result(String customer, BigInteger customerId, String initialBalance, int transactionId, String type, BigInteger amount, String rsCode) {
        this.customer = customer;
        this.customerId = customerId;
        this.initialBalance = initialBalance;
        this.transactionId = transactionId;
        this.type = type;
        this.amount = amount;
        this.rsCode = rsCode;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public BigInteger getCustomerId() {
        return customerId;
    }

    public void setCustomerId(BigInteger customerId) {
        this.customerId = customerId;
    }

    public String getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(String initialBalance) {
        this.initialBalance = initialBalance;
    }

    public int getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public void setAmount(BigInteger amount) {
        this.amount = amount;
    }

    public String getRsCode() {
        return rsCode;
    }

    public void setRsCode(String rsCode) {
        this.rsCode = rsCode;
    }

    @Override
    public String toString() {
        return "Result{" +
                "customer='" + customer + '\'' +
                ", customerId=" + customerId +
                ", initialBalance='" + initialBalance + '\'' +
                ", transactionId=" + transactionId +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", rsCode=" + rsCode +
                '}';
    }
}
