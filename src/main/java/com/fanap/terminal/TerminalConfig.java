package com.fanap.terminal;

import com.fanap.convertor.XmlConvertor;
import com.fanap.convertor.XmlGenerator;
import com.fanap.logger.LoggerConfig;
import com.fanap.model.Transactions;
import com.fanap.repository.Result;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TerminalConfig {
    private final XmlConvertor XML_CONVERTOR = new XmlConvertor();
    private final XmlGenerator xmlGenerator = new XmlGenerator();
    private final List<Transactions> transactionsList = XML_CONVERTOR.xmlParser();
    private Logger logger = LogManager.getLogger();

    public void startTerminal() {
        LoggerConfig.initializeLogger();
        List<Result> resultList = new ArrayList<>();
        for (Transactions transaction : transactionsList) {
            try {
                Socket terminal = new Socket("localhost", 8080);
                ObjectOutputStream out = new ObjectOutputStream(terminal.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(terminal.getInputStream());

                out.writeObject(transaction);

                logger.debug("server port is: " + terminal.getInetAddress());

                Result result = (Result) in.readObject();

                resultList.add(result);
                logger.debug(result.toString() + "\n");

                out.flush();
                out.close();
                in.close();
                terminal.close();
            } catch (IOException | ClassNotFoundException e) {
                logger.debug(e);
            }
        }
        xmlGenerator.resultToXml(resultList);
    }
}