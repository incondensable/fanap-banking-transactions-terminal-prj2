package com.fanap;

import com.fanap.terminal.TerminalConfig;

public class RunTerminal {
    public static void main(String[] args) {
        TerminalConfig terminalConfig = new TerminalConfig();
        terminalConfig.startTerminal();
    }
}